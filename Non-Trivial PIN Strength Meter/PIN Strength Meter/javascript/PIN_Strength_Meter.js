function CommonYears(x) {
	min = 1941;
	max = 2021;
  	return x >= min && x <= max;
}

function DDMM(x){
	DD = x.slice(0,2);
	MM = x.slice(2,4);
	if ((DD>=1 && DD<=29)&&(MM>=1 && MM<=12)){
		return true;
	}
	else if ((DD==30 && (MM>=1 && MM<=12 &&MM!=2))){
		return true;
	}
	else if ((DD==31 && (MM==1 || MM==3 || MM==5 || MM==7 || MM==8 || MM==10 || MM==12))){
		return true;
	}
	else{
		return false;
	}
}

function MMDD(x){
	MM = x.slice(0,2);
	DD = x.slice(2,4);
	if ((DD>=1 && DD<=29)&&(MM>=1 && MM<=12)){
		return true;
	}
	else if ((DD==30 && (MM>=1 && MM<=12 &&MM!=2))){
		return true;
	}
	else if ((DD==31 && (MM==1 || MM==3 || MM==5 || MM==7 || MM==8 || MM==10 || MM==12))){
		return true;
	}
	else{
		return false;
	}
}

function SequentialUp(x){

	d1 = x.slice(0,1);
	d2 = x.slice(1,2);
	d3 = x.slice(2,3);
	d4 = x.slice(3,4);
	if(d1!=0 && (d2-d1==1 && d3-d2==1 && (d4-d3==1 || (d3==9 && d4==0)))){
		return true;
	}

	else{
		return false;
	}

}

function SequentialDown(x){

	d4 = x.slice(0,1);
	d3 = x.slice(1,2);
	d2 = x.slice(2,3);
	d1 = x.slice(3,4);
	if(d1!=0 && (d2-d1==1 && d3-d2==1 && (d4-d3==1 || (d3==9 && d4==0)))){
		return true;
	}

	else{
		return false;
	}

}

function PalindromeCheck(str){
	const len = PIN.length;
	for (let i = 0; i < len / 2; i++) {
        if (PIN[i] !== PIN[len - 1 - i]) {
            return false;
        }
    }
    return true;
}

function CoupletRepeat(x){
	d1 = x.slice(0,2);
	d2 = x.slice(2,4);
	d5 = x.slice(0,1);
	d6 = x.slice(1,2);
	if (d1==d2 && d5!=d6){
		return true;
	}
	return false;
}

function EndWithEightyTwo(x){
	d1 = x.slice(2,4);
	if (d1==88){
		return true;
	}
	return false;
}

function SingleDigitRepeat(x){
	d1 = x.slice(0,1);
	d2 = x.slice(1,2);
	d3 = x.slice(2,3);
	d4 = x.slice(3,4);
	if (d1==d2 && d2==d3 && d3==d4){
		return true;
	}
	return false;
}

function BeginWithFiftyTwo(x){
	d1 = x.slice(0,2);
	if (d1==52){
		return true;
	}
	return false;
}

function UniversalElements(x){
	if (x==5683 || x==4869 || x==1412 || x==7 || x==70){
		return true;
	}
	return false;
}

function ChineseElements(x){
	if (x==1314 || x==3344 || x==5200 || x==5210 || x==9420 || x==8520 || x==5257 || x==8023 || x==7758 || x==9958 || x==2046 || x==9527 || x==3721 || x==8848){
		return true;
	}
	return false;
}
function aabbPattern(x){
	d1 = x.slice(0,1);
	d2 = x.slice(1,2);
	d3 = x.slice(2,3);
	d4 = x.slice(3,4);
	if (d1==d2 && d3==d4 && d1!=d3){
		return true;
	}
	return false;
}

function NumpadPattern(x){
	k1=1;
	k2=2;
	k3=3;
	k4=4;
	k5=5;
	k6=6;
	k7=7;
	k8=8;
	k9=9;
	k0=0;
	d1 = x.slice(0,1);
	d2 = x.slice(1,2);
	d3 = x.slice(2,3);
	d4 = x.slice(3,4);
	//Adjacent patterns
	if (d1==k1 && ((d2==k2 && ((d3==k4 && d4==k8)||(d3==k6 && d4==k8)))||(d2==k4 && d3==k8 && d4==k6))){
		return true;
	}
	else if (d1==k2 && ((d2==k1 && d3==k5 && d4==k7)||(d2==k5 && ((d3==k7 && d4==k0)||(d3==k9 && d4==k0)))||(d2==k3 && d3==k5 && d4==k9))){
		return true;
	}
	else if (d1==k3 && ((d2==k2 && (d3==k4 || d3==k6) && d4==k8)||(d2==k6 && d3==k8 && d4==k4))){
		return true;
	}
	else if (d1==k4 && ((d2==k1 && d3==k5 && d4==k3)||(d2==k5 && d3==k9 && d4==k0)||(d2==k7 && (d3==k5 || d3==k0) && d4==k9))){
		return true;
	}
	else if (d1==k5 && ((d2==k2 && (d3==k4 || d3==k6) && d4==k8)||(d2==k4 && (d3==k2 || d3==k8) && d4==k6)||(d2==k8 && (d3==k4 || d3==k6) && d4==k2)||(d2==k6 && (d3==k2 || d3==k8) && d4==k4))){
		return true;
	}
	else if (d1==k6 && ((d2==k3 && d3==k5 && d4==k1)||(d2==k5 && d3==k7 && d4==k0)||(d2==k9 && (d3==k5 || d3==k0) && d4==k7))){
		return true;
	}
	else if (d1==k7 && ((d2==k4 && (d3==k2 || d3==k8) && d4==k6)||(d2==k8 && d3==k6 && d4==k2))){
		return true;
	}
	else if (d1==k8 && ((d2==k7 && d3==k5 && d4==k1)||(d2==k9 && d3==k5 && d4==k3)||(d2==k0 && (d3==k7 || d3==k9) && d4==k5))){
		return true;
	}
	else if (d1==k9 && ((d2==k8 && d3==k4 && d4==k2)||(d2==k6 && (d3==k8 || d3==k2) && d4==k4))){
		return true;
	}
	else if (d1==k0 && d2==k8 && (d3==k4 || d3==k6) && d4==k2){
		return true;
	}
	// Cross
	else if (d1==k2 && ((d2==k4 && ((d3==k8 && d4==k6)||(d3==k6 && d4==k8)))||(d2==k8 && ((d3==k4 && d4==k6)||(d3==k6 && d4==k4)))||(d2==k6 && ((d3==k4 && d4==k8)||(d3==k8 && d4==k4))))){
		return true;
	}
	else if (d1==k4 && ((d2==k2 && ((d3==k6 && d4==k8)||(d3==k8 && d4==k6)))||(d2==k8 && ((d3==k2 && d4==k6)||(d3==k6 && d4==k2)))||(d2==k6 && ((d3==k2 && d4==k8)||(d3==k8 && d4==k2))))){
		return true;
	}
	else if (d1==k6 && ((d2==k2 && ((d3==k4 && d4==k8)||(d3==k8 && d4==k4)))||(d2==k4 && ((d3==k2 && d4==k8)||(d3==k8 && d4==k2)))||(d2==k8 && ((d3==k2 && d4==k4)||(d3==k4 && d4==k2))))){
		return true;
	}
	else if (d1==k8 && ((d2==k4 && ((d3==k2 && d4==k6)||(d3==k6 && d4==k2)))||(d2==k2 && ((d3==k4 && d4==k6)||(d3==k6 && d4==k4)))||(d2==k6 && ((d3==k2 && d4==k4)||(d3==k4 && d4==k2))))){
		return true;
	}
	else if (d1==k5 && ((d2==k7 && ((d3==k0 && d4==k9)||(d3==k9 && d4==k0)))||(d2==k0 && ((d3==k7 && d4==k9)||(d3==k9 && d4==k7)))||(d2==k9 && ((d3==k7 && d4==k0)||(d3==k0 && d4==k7))))){
		return true;
	}
	else if (d1==k7 && ((d2==k5 && ((d3==k9 && d4==k0)||(d3==k0 && d4==k9)))||(d2==k0 && ((d3==k5 && d4==k9)||(d3==k9 && d4==k5)))||(d2==k9 && ((d3==k5 && d4==k0)||(d3==k0 && d4==k5))))){
		return true;
	}
	else if (d1==k9 && ((d2==k5 && ((d3==k7 && d4==k0)||(d3==k0 && d4==k7)))||(d2==k7 && ((d3==k5 && d4==k0)||(d3==k0 && d4==k5)))||(d2==k0 && ((d3==k5 && d4==k7)||(d3==k7 && d4==k5))))){
		return true;
	}
	else if (d1==k0 && ((d2==k5 && ((d3==k7 && d4==k9)||(d3==k9 && d4==k7)))||(d2==k7 && ((d3==k5 && d4==k9)||(d3==k9 && d4==k5)))||(d2==k9 && ((d3==k5 && d4==k7)||(d3==k7 && d4==k5))))){
		return true;
	}
	//Corners
	else if (d1==k1 && ((d2==k7 && ((d3==k3 && d4==k9)||(d3==k9 && d4==k3)))||(d2==k3 && ((d3==k7 && d4==k9)||(d3==k9 && d4==k7)))||(d2==k9 && ((d3==k3 && d4==k7)||(d3==k7 && d4==k3))))){
		return true;
	}
	else if (d1==k3 && ((d2==k1 && ((d3==k7 && d4==k9)||(d3==k9 && d4==k7)))||(d2==k7 && ((d3==k1 && d4==k9)||(d3==k9 && d4==k1)))||(d2==k9 && ((d3==k1 && d4==k7)||(d3==k7 && d4==k1))))){
		return true;
	}
	else if (d1==k7 && ((d2==k1 && ((d3==k3 && d4==k9)||(d3==k9 && d4==k3)))||(d2==k3 && ((d3==k1 && d4==k9)||(d3==k9 && d4==k1)))||(d2==k9 && ((d3==k1 && d4==k3)||(d3==k3 && d4==k1))))){
		return true;
	}
	else if (d1==k9 && ((d2==k1 && ((d3==k3 && d4==k7)||(d3==k7 && d4==k3)))||(d2==k3 && ((d3==k1 && d4==k7)||(d3==k7 && d4==k1)))||(d2==k7 && ((d3==k1 && d4==k3)||(d3==k3 && d4==k1))))){
		return true;
	}
	//Diagonal Swipe
	else if ((d1==k1 || d1==k3) && d2==k5 && (d3==k7 || d3==k9) && d4==k0){
		return true;
	}
	else if (d1==k0 && (d2==k7 || d2==k9) && d3==k5 && (d4==k1 || d4==k3)){
		return true;
	}
	//Horizontal Swipe
	else if (d1==k1 && d2==k2 && d3==k3 && d4==k5){
		return true;
	}
	else if (d1==k4 && d2==k5 && d3==k6 && (d4==k2 || d4==k8)){
		return true;
	}
	else if (d1==k7 && d2==k8 && d3==k9 && (d4==k5 || d4==k0)){
		return true;
	}
	else if (d1==k3 && d2==k2 && d3==k1 && d4==k5){
		return true;
	}
	else if (d1==k6 && d2==k5 && d3==k4 && (d4==k2 || d4==k8)){
		return true;
	}
	else if (d1==k9 && d2==k8 && d3==k7 && (d4==k5 || d4==k0)){
		return true;
	}
	//Vertical Swipe
	else if (d1==k1 && d2==k4 && d3==k7 && (d4==k5 || d4==k0)){
		return true;
	}
	else if (d1==k2 && ((d2==k0 && ((d3==k5 && d4==k8)||(d3==k8 && d4==k5)))||(d2==k5 && ((d3==k8 && d4==k0)||(d3==k0 && d4==k8)))||(d2==k8 && ((d3==k5 && d4==k0)||(d3==k0 && d4==k5))))){
		return true;
	}
	else if (d1==k3 && d2==k6 && d3==k9 && (d4==k5 || d4==k0)){
		return true;
	}
	else if (d1==k5 && ((d2==k2 && ((d3==k8 && d4==k0)||(d3==k0 && d4==k8)))||(d2==k8 && ((d3==k2 && d4==k0)||(d3==k0 && d4==k2)))||(d2==k0 && ((d3==k2 && d4==k8)||(d3==k8 && d4==k2))))){
		return true;
	}
	else if (d1==k7 && d2==k4 && d3==k1 && d4==k5){
		return true;
	}
	else if (d1==k0 && ((d2==k2 && ((d3==k8 && d4==k5)||(d3==k5 && d4==k8)))||(d2==k8 && ((d3==k2 && d4==k5)||(d3==k5 && d4==k2)))||(d2==k5 && ((d3==k2 && d4==k8)||(d3==k8 && d4==k2))))){
		return true;
	}
	else if (d1==k8 && ((d2==k2 && ((d3==k5 && d4==k0)||(d3==k0 && d4==k5)))||(d2==k5 && ((d3==k2 && d4==k0)||(d3==k0 && d4==k2)))||(d2==k0 && ((d3==k2 && d4==k5)||(d3==k5 && d4==k2))))){
		return true;
	}
	else if (d1==k9 && d2==k6 && d3==k3 && d4==k5){
		return true;
	}
	//Box
	else if (x==1245 || x==2356 || x==4578 || x==5689){
		return true;
	}
}

function ZerotoSix(x){
	d1 = x.slice(0,1);
	d2 = x.slice(1,2);
	d3 = x.slice(2,3);
	d4 = x.slice(3,4);
	if ((d1>=0 && d1<=6)&&(d2>=0 && d2<=6)&&(d3>=0 && d3<=6)&&(d4>=0 && d4<=6)){
		return true;
	}
	return false;
}
function MMYY(x){
	d1 = x.slice(0,2);
	d2 = x.slice(2,4);
	if (d1>=1 && d1<=12 && ((d2>=41 && d2<=99)||(d2>=0 && d2<=21))){
		return true;
	}
	return false;
}

function PIN_Strength_Check(){
	document.getElementById("Strength").innerHTML=null;
	document.getElementById("Reason").innerHTML=null;
	PIN = document.getElementById("PIN").value;
	//Common Years
	if(CommonYears(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Common Year"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);

	}
	//DDMM Check
	if(DDMM(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("DDMM format"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//MMDD Check
	if(MMDD(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("MMDD format"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Sequential Up Check
	if (SequentialUp(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Sequential Up"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Sequential Down Check
	if (SequentialDown(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Sequential Down"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Palindrome Check
	if (PalindromeCheck(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("It's a palindrome"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Couplet Repeat
	if (CoupletRepeat(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Couplet Repeat"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//End with 88
	if (EndWithEightyTwo(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Ends with 88"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Singlet Repeat
	if (SingleDigitRepeat(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("All digits repeated"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Begin with 52
	if (BeginWithFiftyTwo(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Begins with 52"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Universal Elements
	if (UniversalElements(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Universal Element"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Chinese Elements
	if (ChineseElements(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Chinese Element"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//aabb Pattern
	if (aabbPattern(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("aabb Pattern"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Numpad Pattern
	if (NumpadPattern(PIN)){
		document.getElementById("Strength").innerHTML = "Weak";
		var node = document.createElement("LI");
		var textnode = document.createTextNode("Numpad Pattern"); 
		node.appendChild(textnode);
		document.getElementById("Reason").appendChild(node);
	}
	//Moderate PINs
	if(document.getElementById("Strength").innerHTML != "Weak"){
		//Sequential Up Double Digit
		if (ZerotoSix(PIN)){
			document.getElementById("Strength").innerHTML = "Moderate";
			var node = document.createElement("LI");
			var textnode = document.createTextNode("Only digits between 0 to 6 are used"); 
			node.appendChild(textnode);
			document.getElementById("Reason").appendChild(node);
		}
		if (MMYY(PIN)){
			document.getElementById("Strength").innerHTML = "Moderate";
			var node = document.createElement("LI");
			var textnode = document.createTextNode("MMYY Pattern"); 
			node.appendChild(textnode);
			document.getElementById("Reason").appendChild(node);
		}

	}

	if(document.getElementById("Strength").innerHTML != "Weak" && document.getElementById("Strength").innerHTML != "Moderate"){
		document.getElementById("Strength").innerHTML = "Strong";

	}

	if(document.getElementById("Strength").innerHTML=="Weak"){
	document.getElementById("Strength").style.background="#f32013";
	}
	else if(document.getElementById("Strength").innerHTML=="Moderate"){
	document.getElementById("Strength").style.background="#ff7f00";
	}
	else if(document.getElementById("Strength").innerHTML=="Strong"){
	document.getElementById("Strength").style.background="#32c832";
	}
}
